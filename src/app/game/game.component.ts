import { Component, OnInit } from '@angular/core';
import { HandService } from '../hand.service';
import { Hands } from '../hands.model';
import { Hand } from '../hand.model';
import { Card } from '../card.model';
import { AppState } from '../app.state';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.less']
})
export class GameComponent implements OnInit {

  constructor(protected handService: HandService) { }

  // public hands: Hands = {};

  public debug = false;

  public fresh = true;

  public gameOn = false; // a simple check to see whether the game is over or not

  public winner = 'Push the Buttons';
  public computerHand: Hand;
  public playerHand: Hand;

  public playerWins = 0;
  public playerTies = 0;
  public playerLosses = 0;

  public user = 'loading...';
  public message = 'Enter a unique name and push the New Game button!';

  public email;
  public emailEntered = false;

  public loading = false;

  ngOnInit(): void {
    console.log('Doge on tärkeä. Sen täytyy olla favicon >:O');
    this.resetHands();
  }

  public loginUser() {

    this.message = 'Loggin User "' + this.email + '" in...';

    // Log User in using the provided email
    this.handService.login(this.email).then((response) => {
      console.log('User Logged In');

      localStorage.setItem('_token', response.access_token);
      AppState.User.token = response.access_token;

      this.message = 'User Logged in as ' + this.email;
      console.log('Below should be the new access token');
      console.log(response);
      console.log(localStorage._token);
      console.log('The same should be in localStorage');

      this.getStats();

      this.emailEntered = true;

      // Start the new Game after the user is logged in

      this.startGame();

    }).catch((error) => {
      this.message = 'Loggin User in Totally Failed!';
      console.error(error);
    });


  }

  public getUser() {
    this.message = 'Checking User information...';
    this.handService.status().then((response) => {
      console.log('User fetched');
      this.message = 'User is ' + response.current_user;
      console.log(response);
      this.user = response.current_user;
      AppState.User.name = response.current_user;
    }).catch((error) => {
      console.error(error);
      this.user = 'Not Logged In';
      this.message = 'User is ' + this.user;
      AppState.User.name = this.user;
    });
  }

  public newGame() {
    // Check if email is set
    if (!this.email) {
      alert('Please enter your email!');
      return false;
    }

    this.message = 'Starting a New Game...';
    this.loading = true;
    this.winner = '';
    this.resetHands();
    this.fresh = true;

    if (!this.emailEntered) {
      // Login as new game starts
      this.loginUser();
    } else {
      this.startGame();
    }

  }

  public resetHands() {
    this.computerHand = new Hand( new Card( '0', 0 ), new Card( '0', 0 ) );
    this.playerHand = new Hand( new Card( '0', 0 ), new Card( '0', 0 ) );
  }

  public startGame() {

    this.handService.newGame().then((response) => {
      this.gameOn = true;
      console.log('New Game started');
      this.message = 'New Game Started!';
      console.log(response);



      // Get starting hands
      this.getHands();

    }).catch((error) => {
      this.message = 'Starting a New Game Failed!';
      console.error(error);
      this.loginUser(); // Try to Log in again
    });
  }

  public hit() {
    this.message = 'Hitting in the face...';
    this.loading = true;

    this.handService.hit().then((response) => {
      this.message = 'In the Face!';

      this.getHands();

    }).catch((error) => {
      this.message = 'The hit missed.';
      console.log(error);
    });
  }

  public stay() {
    this.message = 'Ending the game here...';
    this.loading = true;

    this.handService.stay().then((response) => {
      this.message = 'You stayed!';

      this.getHands();

    }).catch((error) => {
      this.message = 'Well you can\'t stay.';
      console.error((error));
    });
  }

  public getHands() {

    this.loading = true;

    this.message = 'Getting hands...';
    this.handService.getHand().then((response) => {
      this.message = 'The hands are up!';
      console.log(response);

      this.playerHand = response.player_hand;
      this.computerHand = response.computer_hand;
      console.log('Player Hand', this.playerHand);

      const that = this;

      // delay the winner declaration
      setTimeout(() => {
        that.winner = response.winner;
        that.loading = false;

        if (response.winner === 'computer' || response.winner === 'player' || response.winner === 'tie') {
          that.gameOn = false;
          that.fresh = false;

          that.getStats();
        }
      }, 2000);



    }).catch((error) => {
      this.message = 'Things went out of hand';
      console.error(error);
    });
  }

  public getStats() {
    this.handService.stats().then((response) => {
      this.playerWins = response.player_wins;
      this.playerTies = response.player_ties;
      this.playerLosses = response.player_loses;
    }).catch((error) => {
      console.error(error);
    });
  }

}
