import { Card } from './card.model';

export class Hand {
  constructor(
    card1: Card,
    card2: Card,
    card3?: Card,
    card4?: Card,
    card5?: Card
  ) {}
  public card1: Card;
  public card2: Card;
  public card3: Card;
  public card4: Card;
  public card5: Card;
}
