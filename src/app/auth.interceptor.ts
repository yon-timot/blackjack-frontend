import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Api } from './api';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  private authHeader: string = '';

  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // If there is no stored token in local storage, or the request url is something
    // else than the main API, just go with the normal headers.

    if (!localStorage._token || !req.url.includes(Api.URL)) return next.handle(req);

    // Make a clone of the request but add the auth token to the headers

    this.authHeader = 'Bearer ' + localStorage._token;

    const authReq = req.clone({headers: req.headers.set('Authorization', this.authHeader)});
    // Pass on the cloned request instead of the original request.
    return next.handle(authReq);
  }
}
