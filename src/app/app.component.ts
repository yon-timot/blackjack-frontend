import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'blackjack';

  public buttonVisible = true;
  public buttonText = 'Uusi Peli';

  hideButton() {
    return this.buttonVisible = false;
  }
}
