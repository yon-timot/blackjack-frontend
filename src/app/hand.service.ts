import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Api } from './api';
import { Hand } from './hand.model';
import { AppState } from './app.state';
import { AuthInterceptor } from './interceptors/auth.service';

@Injectable({
  providedIn: 'root'
})
export class HandService {

  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(protected http: HttpClient) {}

  public login(email: string): Promise<any> {
    return this.http.post(Api.URL + 'login', {email}, this.httpOptions).toPromise();
  }

  public status(): Promise<any> {

    return this.http.get(Api.URL, this.httpOptions).toPromise();
  }

  public newGame(): Promise<any> {

    return this.http.post(Api.URL + 'move', {new_game: 'true'}, this.httpOptions).toPromise();
  }

  public hit(): Promise<any> {

    return this.http.post(Api.URL + 'move', {move: 'hit'}, this.httpOptions).toPromise();
  }

  public stay(): Promise<any> {

    return this.http.post(Api.URL + 'move', {move: 'stay'}, this.httpOptions).toPromise();
  }

  public getHand(): Promise<any> {

    return this.http.post(Api.URL + 'status', {get_hand: 'both'}, this.httpOptions).toPromise();
  }

  public stats(): Promise<any> {

    return this.http.get(Api.URL + 'stats').toPromise();
  }

  protected appendToken() {

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + AppState.User.token,
      })
    };
  }

}
